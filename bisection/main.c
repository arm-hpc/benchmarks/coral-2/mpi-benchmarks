/* 
   Argonne Leadership Computing Facility benchmark
   BlueGene/P version
   Bi-section bandwidth
   Written by Vitali Morozov <morozov@anl.gov>
   Modified by Daniel Faraj <faraja@us.ibm.com>
   
   Measures a partition minimal bi-section bandwidth
*/    
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

#define MAXN 15000         // repetition rate for a single pair test
#define LENGTH 524288

main( int argc, char *argv[] )
{
  int ppn, taskid, ntasks, is1, ir1, i, k;
  char *sb, *rb;
  double d, d1;
  MPI_Status stat[2];
  MPI_Request req[2];
  double t1;
 
  posix_memalign( (void **)&sb, 16, sizeof( char ) * LENGTH );
  posix_memalign( (void **)&rb, 16, sizeof( char ) * LENGTH );

  MPI_Init( &argc, &argv );
  MPI_Comm_rank( MPI_COMM_WORLD, &taskid );
  MPI_Comm_size( MPI_COMM_WORLD, &ntasks );
   
  if ( getranks( taskid, &is1, &ir1, &ppn ) == 0 )
  {
    //fprintf( stderr,"%d: %d => %d\n", taskid, is1, ir1 );
        
    d = 0.0;
        
    MPI_Barrier (MPI_COMM_WORLD);

    if ( taskid == is1 )
    {
      t1 = MPI_Wtime();
      for ( k = 0; k < MAXN; k++ )
      {  
        MPI_Isend( sb, LENGTH, MPI_BYTE, ir1, is1, MPI_COMM_WORLD, &req[0] );
        MPI_Irecv( rb, LENGTH, MPI_BYTE, ir1, ir1, MPI_COMM_WORLD, &req[1] );
        MPI_Waitall( 2, req, stat );
      }
      t1 = MPI_Wtime() - t1;
      
      t1 /= (double)MAXN;
      d = 2 * (double)LENGTH / t1;
    }

    if ( taskid == ir1 )
    {
      for ( k = 0; k < MAXN; k++ )
      {
        MPI_Isend( sb, LENGTH, MPI_BYTE, is1, ir1, MPI_COMM_WORLD, &req[0] );
        MPI_Irecv( rb, LENGTH, MPI_BYTE, is1, is1, MPI_COMM_WORLD, &req[1] );
        MPI_Waitall( 2, req, stat );
      }
    }

    MPI_Reduce( &d, &d1, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD );
        
    if ( taskid == 0 )
      printf("PPN: %d Bisection BW(GB/s): %.2lf\n", ppn, d1 / 1e9 );
  }

  MPI_Finalize();
  free( sb );
  free( rb );
}
