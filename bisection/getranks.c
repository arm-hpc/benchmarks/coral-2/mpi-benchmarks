
#include "mpi.h"
#include "mpix.h"

/* Returns two communication tasks, participating in bi-section test
   1) Determine the partition size (X,Y,Z)
   2) Find the largest dimension S. This dimension will cut the partition in
      two halves
   3) Determine my coodinates (x,y,z,t) 
   
   a) S = X. Participating sending tasks (0,y,z,t)
   will communicate with receiveng tasks (1,y,z,t), other tasks are ignored.   
   b) S = Y. Participating sending tasks (x,0,z,t)
   will communicate with receiveng tasks (x,1,z,t), other tasks are ignored.   
   c) S = Y. Participating sending tasks (x,y,0,t)
   will communicate with receiveng tasks (x,y,1,t), other tasks are ignored.   
        
   Returns:
        0, if source and target are assigned for participating tasks or set -1
        to nonparticipating tasks
        1, if cannot assign a target, nothing runs on the core
        1, if cannot assign a source, nothing runs on the core
*/


int getranks(int rank, int *source, int *target, int *ppn)
{
  int i, max, longest;
  MPIX_Hardware_t hw;
  MPIX_Hardware(&hw);

  *ppn = hw.ppn;
  
  max = hw.Size[0];
  longest = 0;
  for (i = 1; i < hw.torus_dimension; i++)
    if (hw.Size[i] > max)
    {
      longest = i;
      max = hw.Size[i];
    }
  
  int * coords = (int*) malloc(sizeof(int) * (hw.torus_dimension+1));
  MPIX_Rank2torus(rank,&coords[0]);
  coords[hw.torus_dimension] = hw.coreID;
  
  // for non-participating tasks
  *source = -1;
  *target = -1;

  // sender
  if (hw.Coords[longest] == 0)
  {
    *source = rank;
    coords[longest] = 1 % hw.Size[longest];
    MPIX_Torus2rank(coords, target);
  }
  
  if (hw.Coords[longest] == hw.Size[longest] / 2)
  {
    *source = rank;
    coords[longest] = (hw.Size[longest] / 2 + 1) % hw.Size[longest];
    MPIX_Torus2rank(coords, target);
  }
  
  // receiver
  if (hw.Coords[longest] == 1)
  {
    *target = rank;
    coords[longest] = 0;
    MPIX_Torus2rank(coords, source);
  }
   
  if (hw.Coords[longest] == hw.Size[longest] / 2 + 1)
  {
    *target = rank;
    coords[longest] = (hw.Size[longest] / 2) % hw.Size[longest];
    MPIX_Torus2rank(coords, source);
  }

  free(coords);
  return 0;
}
