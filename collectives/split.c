#include "mpi.h"
#include "mpix.h"

/* 
   Returns two communicators, which cut a partition in two halves at coordinate c
   c = 0 - coordinate X
   c = 1 - coordinate Y
   c = 2 - coordinate Z
    
   Returns:
        0, if split was successful
        1, if given incorrect c
*/

#if 0
int split( int c, MPI_Comm *half0, MPI_Comm *half1, int *ppn )
{
  MPIX_Hardware_t hw;
  MPIX_Hardware(&hw);
  *ppn = hw.ppn;
  
  int size, rank;
  int Nlist0, *list0, Nlist1, *list1;
  MPI_Group World_group, Comm0_group, Comm1_group;

  int * coords = (int*) malloc(sizeof(int) * (hw.torus_dimension+1));
  
  /* Make 2 lists of ranks: list0 to exclude from half0,
     list1 to exclude from list 1 */
  MPI_Comm_size( MPI_COMM_WORLD, (int *)&size );
  list0 = (int*) malloc( sizeof( int ) * size );
  list1 = (int*) malloc( sizeof( int ) * size );

  Nlist0 = 0;
  Nlist1 = 0;

  switch ( c )
  {
      case ( 0 ):
          for ( rank = 0; rank < size; rank++ )
          {
              MPIX_Rank2torus(rank, &coords[0]);
              if (coords[0] < hw.Size[0] / 2 ) 
                  list1[ Nlist1++ ] = rank;
              else
                  list0[ Nlist0++ ] = rank;
          }
          break;
      case ( 1 ):
          for ( rank = 0; rank < size; rank++ )
          {
              MPIX_Rank2torus(rank, &coords[0]);
              if (coords[1] < hw.Size[1] / 2 ) 
                  list1[ Nlist1++ ] = rank;
              else
                  list0[ Nlist0++ ] = rank;

          }
          break;
      case ( 2 ):
          for ( rank = 0; rank < size; rank++ )
          {
              MPIX_Rank2torus(rank, &coords[0]);
              if (coords[2] < hw.Size[2] / 2 ) 
                  list1[ Nlist1++ ] = rank;
              else
                  list0[ Nlist0++ ] = rank;
          }
          break;
      case ( 3 ):
          for ( rank = 0; rank < size; rank++ )
          {
              MPIX_Rank2torus(rank, &coords[0]);
              if (coords[3] < hw.Size[3] / 2 ) 
                  list1[ Nlist1++ ] = rank;
              else
                  list0[ Nlist0++ ] = rank;
          }
          break;
      case ( 4 ):
          for ( rank = 0; rank < size; rank++ )
          {
              MPIX_Rank2torus(rank, &coords[0]);
              if (coords[4] < hw.Size[4] / 2 ) 
                  list1[ Nlist1++ ] = rank;
              else
                  list0[ Nlist0++ ] = rank;
          }
          break;
          
     default:
          free( list0 ); 
          free( list1 ); 
          free( coords );
          return 1;
  }    

  MPI_Comm_group( MPI_COMM_WORLD, &World_group );
  MPI_Group_excl( World_group, Nlist0, list0, &Comm0_group );
  MPI_Group_excl( World_group, Nlist1, list1, &Comm1_group );
  MPI_Comm_create( MPI_COMM_WORLD, Comm0_group, half0 );
  MPI_Comm_create( MPI_COMM_WORLD, Comm1_group, half1 );
  MPI_Group_free( &Comm0_group );
  MPI_Group_free( &Comm1_group );

  free( list0 );
  free( list1 );
  free( coords );
  return 0;
}

#endif


int split(int c, MPI_Comm *newcomm, int *ppn, int *color)
{
  MPIX_Hardware_t hw;
  MPIX_Hardware(&hw);
  *ppn = hw.ppn;

  if (c == -1)
    return 0;
  
  int size, rank, key;
  int * coords = (int*) malloc(sizeof(int) * (hw.torus_dimension+1));

  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPIX_Rank2torus(rank, &coords[0]);
  
  *color = 1;
  key = rank;

  // c >= and < 5 on 5D torus
  if (coords[c] < hw.Size[c] / 2)
    *color = 0;

  MPI_Comm_split(MPI_COMM_WORLD, *color, key, newcomm);
  free(coords);

  return 0;
}
